# Image Tagging Project


## Dependencies
### Environment
This is targeted at Python 3.6 and above.
```bash
$ sudo apt install python3-venv
$ echo This installs the venv module.

$ echo Create an Environments folder
$ mkdir ~/environments
$ cd ~/environments
$ echo Choose a name for your environment, such as 'Py3.6'
$ python3 -m venv Py3.6
```
View the contents of the directory with `python3 -m venv Py3.6`.  Finally, activate the environment using `source ~/environments/Py3.6`.  

Now load this envrionment every time before working on the project, and there will be no impact if you install newer or different versions of Python in the future.

You should also see that your prompt is changed to include the name of the environment.

### Modules

```python
from os import path
import sys
from tkinter.ttk import *
from tkinter import *
import logging
#from typing import List
from scrollImage import ScrollableImage
from dotenv import dotenv_values
import mysql.connector
```

Install the modules for tkinter, tkinter.ttk, dotenv_values and mysql.connector