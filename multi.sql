select Images.ID, Tags.tag from Images inner join Tags on Images.id = Tags.Image_ID order by Tags.tag, Images.ID;


select I.ID from Images I
  INNER JOIN Tags t0 on t0.Image_ID = I.ID
  INNER JOIN Tags t1 on t1.Image_ID = I.ID
  WHERE t0.tag = 'nature' AND t1.tag='flowers'
  ORDER BY I.ID;


SELECT I.ID, I.Photo_Path from Images I 
            INNER JOIN Tags t0 on t0.Image_ID = I.ID
            WHERE t0.tag = 'nature'
            ORDER BY I.ID;
