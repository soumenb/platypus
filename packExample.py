#!/usr/bin/python3

# Importing tkinter module
#from os import pathconf_names, stat

from ast import Return
from cgitb import enable
from cmath import exp
import numbers
from os import path
import sys
from textwrap import fill
from tkinter import messagebox
from tkinter.ttk import *
from tkinter import *
import logging
# from typing import List
# from scrollImage import ScrollableImage
from dotenv import dotenv_values
import mysql.connector

SECRET_CONFIG = 'env.secret'
widgets = {}
search_results = {}

def init():

    # if env.secret is missing, show error and exit
    if not (path.exists(SECRET_CONFIG) and path.isfile(SECRET_CONFIG)):
        print(f'Missing secret file: {SECRET_CONFIG}, exiting.')
        print (
                '''Create a file with the following values'
                DB_HOST=localhost'
                DB_USER=root'
                DB_PASSWORD=<password, duh!>'
                DB_DATABASE=PROJECT'
                ''')
        sys.exit(1)

    global config 
    config =    {
        **dotenv_values('env.shared'),
        **dotenv_values(SECRET_CONFIG)
    }

    print (config)


    logging.basicConfig(
        filename=config['LOG_FILE'],
        filemode='a',
        level=logging.DEBUG,
        format=config['LOG_FORMAT'])

    global logger
    logger = logging.getLogger('packExample')
    logger.info('Invoked from main')

    global images_db
    images_db = mysql.connector.connect(
        host     = config['DB_HOST'],
        user     = config['DB_USER'],
        passwd   = config['DB_PASSWORD'],
        database = config['DB_DATABASE']
    )

    global db_cursor
    db_cursor = images_db.cursor(buffered=True, dictionary=True)


'''
    Widgets: paned_window
        search_pane
            search_tags_pane
                st1_frame
                    search_tag_entry
                    add_search_tag
                st2_frame
                    delete_search_tag
                    spacer_1_label
                    search_with_tags
                search_tags_listbox
            results_pane
                results_listbox
        image_pane
            image_details_pane
                id1_frame
                    label_id
                    entry_id
                id2_frame
                    label_path
                    entry_path
                id3_frame
                    label_tags
                    entry_tags
                id4_frame
                    image_tag_entry
                    add_image_tag
                    delete_image_tag
            image_display_pane
                current_image
'''

def checkdb():
    
    db_cursor = images_db.cursor(buffered=True, dictionary=True)
    
    statement = 'SELECT Photo_Path from Images'
    db_cursor.execute(statement)

    result = db_cursor.fetchall()

    for path in result:
        print(path)


def add_tag_for_search():
    print ("Button: add_search_tag")
    
    new_tag = widgets['search_tag_entry'].get()
    widgets['tag_listbox'].insert(END, new_tag)

def delete_search_tag2():
    print('Button: delete_search_tag2')

    index_to_delete = widgets['tag_listbox'].curselection()[0]
    print(index_to_delete)

    widgets['tag_listbox'].delete(index_to_delete)


def statement_for_all_tags_in_search(number_search_tags):
    
    # do first tag

    statement_first_part = '''SELECT I.ID, I.Photo_Path from Images I 
            INNER JOIN Tags t0 on t0.Image_ID = I.ID'''
    
    statement_second_part = """
            WHERE t0.tag=%s"""

    statement_third_part = '''
            ORDER BY I.ID'''

    # do remaining tags
    for i in range(1, number_search_tags):
        statement_first_part += f'''
            INNER JOIN Tags t{i} on t{i}.Image_ID = I.ID'''
        statement_second_part += f" AND t{i}.tag = %s"

    return statement_first_part + statement_second_part + statement_third_part




def statement_for_any_tag_in_search(number_search_tags):
    # do first tag
    selected_tag_pics_statement = '''
            SELECT ID, Photo_Path, Tag
                FROM Tags
                INNER JOIN Images
                    ON Tags.Image_ID = Images.ID
                WHERE Tag in (%s'''

    # do remaining tags
    for i in range(1, number_search_tags):
        selected_tag_pics_statement += ', %s'

    # close statement
    selected_tag_pics_statement += ')'

    return selected_tag_pics_statement


def search_with_tags():
    
    # get comma separated list of tags from search_tag_entry
    
    search_tags = widgets['stringvar_search_tags'].get()

    search_tags_tuple = tuple(set( [ t.strip() for t in search_tags.split(',')]))

    print (search_tags_tuple)

    number_search_tags = len(search_tags_tuple)

    # if no search tags
    if number_search_tags == 1 and not search_tags_tuple[0]:
        select_all_pics_statement = 'SELECT ID, Photo_Path from Images'
        db_cursor.execute(select_all_pics_statement)
        print(db_cursor.rowcount)
 
    else:
        # if more than one

        # do first tag
        # selected_tag_pics_statement = statement_for_any_tag_in_search(number_search_tags)
        selected_tag_pics_statement = statement_for_all_tags_in_search(number_search_tags)
        print(selected_tag_pics_statement)

        try:
            db_cursor.execute(selected_tag_pics_statement, search_tags_tuple)
        except:
            print(db_cursor.rowcount)
            print(db_cursor.statement)

        # print(statement_for_all_tags_in_search(number_search_tags))

    # display results

    print('Button: search_with_tags')
    """ 
    tag_listbox = widgets['tag_listbox']

    # find num of elements in tags box
    # if there are 0 elements, fetch all images
    
    if tag_listbox.size() == 0:
        print('No Tags')
        select_all_pics_statement = 'SELECT ID, Photo_Path from Images'
        db_cursor.execute(select_all_pics_statement)
        print(db_cursor.rowcount)
    
    # else get first element of listbox
    else:
        tags = []

        for i in range(tag_listbox.size()):
            tags.append(tag_listbox.get(i))
        
        tagstring = "', '".join(tags)
        print(tagstring)
        
        





        tag_value = tag_listbox.get(0)
        print(tag_value)
    
        # then search all pictures with that tag
        selected_tag_pics_statement = '''
        SELECT ID, Photo_Path, Tag
        FROM Tags
        INNER JOIN Images
        ON Tags.Image_ID = Images.ID
        WHERE Tag in (%s)'''

        print(tagstring)

        db_cursor.execute(selected_tag_pics_statement, (tagstring,))
        print(db_cursor.rowcount)
        print(db_cursor.statement) """


    results = db_cursor.fetchall()

    search_results.clear()

    results_listbox = widgets['results_listbox']

    results_listbox.delete(0, results_listbox.size())

    for result in results:
        path_result = result['Photo_Path']

        results_listbox.insert(END, path_result)

        search_results[path_result] = result
        print(result)
    





def add_tag_to_image():
    tag_text = widgets['stringvar_tag_entry'].get()
    print('add tag to image id', tag_text)

    if tag_text == '':
        messagebox.showinfo('Missing Field', 'Tag value required')

    # execute sql statement to insert 
    current_id = widgets['stringvar_id'].get()

    insert_tag_statement = '''INSERT INTO Tags (Image_ID, Tag) VALUES (%s, %s)'''
    val = (current_id, tag_text)

    try:
        db_cursor.execute(insert_tag_statement, val)
        images_db.commit()
        print (db_cursor.rowcount, 'record inserted')
    except:
        messagebox.showinfo("DB Not Updated", "Tag Already Exists")
    

    widgets['stringvar_tag_entry'].set('')
    display_current_tags(current_id)





def delete_tag_from_image():
    tag_text = widgets['stringvar_tag_entry'].get()
    print('delete tag from image', tag_text)

    if tag_text == '':
        messagebox.showinfo('Missing Field', 'Tag value required')
    
    current_id = widgets['stringvar_id'].get()

    delete_tag_statement = '''DELETE FROM Tags
    WHERE Image_ID = %s and Tag = %s'''
    val = (current_id, tag_text)

    try:
        db_cursor.execute(delete_tag_statement, val)
        images_db.commit()
        print(db_cursor.rowcount, 'record deleted')
    except:
        messagebox.showinfo("DB Not Updated", f"Tag ('{tag_text}') Does Not Exist('{current_id}')")

    widgets['stringvar_tag_entry'].set('')
    display_current_tags(current_id)




def result_selected(event):
    results = widgets['results_listbox']

    selected_tuple = results.curselection()
    
    if len(selected_tuple) == 0:
        print("No result selected")
        return

    results_file = results.get(selected_tuple[0])

    print('Image selected: {results_file}')

    new_image = PhotoImage(file=results_file)
    widgets['image_label'].configure(image=new_image)
    widgets['image_label'].image = new_image
    
    #set the id and path
    # find the right result using key: results_file
    #   next, find the ID in this result

    result =  search_results[results_file]
    id = result['ID']

    print(result, id)

    stringvar_path = results_file

    stringvar_id = widgets['stringvar_id']
    stringvar_id.set(id)

    stringvar_path = widgets['stringvar_path']
    stringvar_path.set(results_file)

    display_current_tags(id)
    
def display_current_tags(id):
    current_id = widgets['stringvar_id'].get()

    select_tags = '''SELECT Tag from Tags WHERE Image_ID = %s'''
    val = (current_id,)

    db_cursor.execute(select_tags, val)

    results = db_cursor.fetchall()
    print(results)

    current_tags = []
    for result in results:
        current_tags.append(result['Tag'])

    widgets['stringvar_current_tags'].set(', '.join(current_tags))






def createTwoPanes1_1():

    print('''
    ☑ Widgets: paned_window
        ☑ search_frame
            ☑ search_tags_pane
                ☑ st1_frame
                    ☑ search_tag_entry
                    ☑ add_search_tag
                ☑ st2_frame
                    ☑ delete_search_tag
                    ☑ spacer_1_label
                    ☑ search_with_tags
                ☑ search_tags_listbox
            ☑ results_pane
                ☑ results_listbox
        ☑  image_frame
            ☑ image_details_pane
                ☑ id1_frame
                    ☑ label_id
                    ☑ entry_id
                ☐ id2_frame
                    ☐ label_path
                    ☐ entry_path
                ☐ id3_frame
                    ☐ label_tags
                    ☐ entry_tags
                ☐ id4_frame
                    ☐ image_tag_entry
                    ☐ add_image_tag
                    ☐ delete_image_tag
            ☐ image_display_pane
                ☐ current_image
''')

    # create master window
    root = Tk()
    widgets['root'] = root

    stringvar_id = StringVar()
    stringvar_path = StringVar()
    stringvar_tag_entry = StringVar()
    stringvar_current_tags = StringVar()
    stringvar_search_tags = StringVar()

    widgets['stringvar_id'] = stringvar_id
    widgets['stringvar_path'] = stringvar_path
    widgets['stringvar_tag_entry'] = stringvar_tag_entry
    widgets['stringvar_current_tags'] = stringvar_current_tags
    widgets['stringvar_search_tags'] = stringvar_search_tags


    stringvar_id.set('--')
    stringvar_path.set('---')
    stringvar_tag_entry.set('-- --')

    print('Path is set to', stringvar_path.get())
    print('ID is set to', stringvar_id.get())
    print('Current Tag is', stringvar_tag_entry.get())



    # Create a paned window that can expand, with two horizontal panes
    paned_window = PanedWindow(orient=HORIZONTAL)
    paned_window.pack(side=TOP, fill=BOTH, expand=True)

    # Labeled frame
    search_frame = LabelFrame(paned_window, text="Tags to search")
    search_frame.pack(side=TOP, fill=BOTH, expand=True)

    # New code starts here =========================================================

    st1_frame = Frame(search_frame)
    st1_frame.pack(side=TOP, fill=BOTH, expand=False)

    search_tag_entry = Entry(st1_frame, textvariable=stringvar_search_tags)
    search_tag_entry.pack(side=LEFT, fill=BOTH, expand=True)
    widgets['search_tag_entry'] = search_tag_entry

    """ 
    add_search_tag = Button(st1_frame, text='Add tag', command=add_tag_for_search)
    add_search_tag.pack(side=LEFT, fill=Y, expand=False) """


    st2_frame = Frame(search_frame)
    st2_frame.pack(side=TOP, fill=BOTH, expand=False)

    """ 
    delete_search_tag_button = Button(st2_frame, text = 'Delete Tag', command=delete_search_tag2)
    # widgets['']               (since button is one-time use)

    delete_search_tag_button.pack(side=LEFT, fill=BOTH, expand=True)

    spacer_label = Label(st2_frame, text='    ')
    spacer_label.pack(side=LEFT, fill=BOTH, expand=True) """

    search_with_tags_button  = Button(st2_frame, text = 'Search with Tags', command=search_with_tags)
    search_with_tags_button.pack(side=LEFT, fill=BOTH, expand = True)

    """ 
    tag_list_frame = Frame (search_frame)
    tag_list_frame.pack(side=TOP, fill=BOTH, expand=False)


    tag_listbox = Listbox (tag_list_frame, height=3, selectmode=SINGLE)
    widgets['tag_listbox'] = tag_listbox


    tag_scrollbar_y = Scrollbar (tag_list_frame, orient=VERTICAL, command=tag_listbox.yview)

    tag_scrollbar_x = Scrollbar (tag_list_frame, orient=HORIZONTAL, command=tag_listbox.xview)

    tag_scrollbar_x.pack(fill=X, expand=False, side=BOTTOM)
    tag_listbox.pack(fill=BOTH, side=LEFT, expand=True)
    tag_scrollbar_y.pack(fill=Y, expand=False, side=RIGHT) """

    results_pane = LabelFrame(search_frame, text='Results')
    results_pane.pack(side=TOP, fill=BOTH, expand=True)

    results_listbox = Listbox (results_pane, height=5, selectmode=SINGLE)
    widgets['results_listbox'] = results_listbox

    results_listbox.bind('<<ListboxSelect>>', result_selected)

    # results_scrollbar_x
    results_scrollbar_x = Scrollbar (results_pane, orient=HORIZONTAL, command=results_listbox.xview)
    # results_scrollbar_y
    results_scrollbar_y = Scrollbar (results_pane, orient=VERTICAL, command=results_listbox.yview)

    results_scrollbar_x.pack (fill=X,      expand=False, side=BOTTOM)
    results_listbox.    pack (fill=BOTH,   expand=True,  side=LEFT)
    results_scrollbar_y.pack (fill=Y,      expand=False, side=RIGHT)


    paned_window.add(search_frame)

    # Labeled frame - Image
    image_frame = LabelFrame(paned_window, text='Image Details')
    image_frame.pack(side=TOP, fill=BOTH, expand=True)



    id1_frame = Frame(image_frame)
    id1_frame.pack(side=TOP, fill=BOTH, expand=False)

    label_id = Label(id1_frame, text='ID:     ')
    label_id.pack(side=LEFT, fill=X, expand=False)

    entry_id = Entry(id1_frame, textvariable=stringvar_id, state='readonly')
    entry_id.pack(side=LEFT, fill=X, expand=True)

    widgets['entry_id'] = entry_id

    id2_frame = Frame(image_frame)
    id2_frame.pack(side=TOP, fill=BOTH, expand=False)

    label_path = Label(id2_frame, text='Path: ')
    label_path.pack(side=LEFT, fill=X, expand=False)

    entry_path = Entry(id2_frame, textvariable=stringvar_path, state='readonly')
    entry_path.pack(side=LEFT, fill=X, expand=True)

    widgets['entry_path'] = entry_path
    
    id3_frame = Frame(image_frame)
    id3_frame.pack(side=TOP, fill=BOTH, expand=False)

    label_tags = Label(id3_frame, text='Tags:')
    label_tags.pack(side=LEFT, fill=X, expand=False)

    entry_tags = Entry(id3_frame, textvariable=stringvar_current_tags, state='readonly')
    entry_tags.pack(side=LEFT, fill=X, expand=True)



    id4_frame = Frame(image_frame)
    id4_frame.pack(side=TOP, fill=BOTH, expand=False)

    image_tag_entry = Entry(id4_frame, textvariable=stringvar_tag_entry)
    image_tag_entry.pack(side=LEFT, fill=X, expand=False)

    add_image_tag = Button(id4_frame, text='Add Tag to Image', command=add_tag_to_image)
    add_image_tag.pack(side=LEFT, fill=X, expand=False)

    delete_image_tag = Button(id4_frame, text='Delete Tag from Image', command=delete_tag_from_image)
    delete_image_tag.pack(side=LEFT, fill=X, expand=False)



    img = PhotoImage(file="captain.jpg")
    image_label = Label(image_frame, image=img)
    widgets['image_label'] = image_label
    image_label.pack(side=BOTTOM, fill=BOTH, expand=True)

    """ 
    image_pane = ScrollableImage(image_frame, image=img, scrollbarwidth=10, width=600, height=300)
    widgets['image_pane']=image_pane
    image_pane.pack(side=BOTTOM, fill=BOTH, expand=True) """






# New code ends here  ==========================================================

    paned_window.add(image_frame)


    for w in widgets:
        print ("Widgets saved are:", w)

    mainloop()

    return



def add_tag_to_search():
    print('In add_tag_to_search()')
    t = widgets['new_search_tag'].get()

    # get all the current values of listbox, make sure this is not a duplicate.
    # See https://www.tutorialspoint.com/python/tk_listbox.htm
    
    search_tag_listbox = widgets['tag_listbox']
    
    # Use <listbox>.size for the number of elements in the listbox.
    # Loop over all the elements in the listbox
    
    all_results = search_tag_listbox.get(0, search_tag_listbox.size() -1)

    for i, s in enumerate (all_results):
        print ("Index", i, ": ", s)
        # TODO: check if new tag is already added? return?

    search_tag_listbox.insert(END, t)


    return


def delete_search_tag():
    # use <listbox>.curselection() for a tuple of (<index>, <selected text>)
    # use <listbox>.delete()

    search_tag_listbox = widgets['tag_listbox']

def search_selected_tags():

    search_tag_listbox = widgets['tag_listbox']

    # get all the tags from search_tag_listbox
    
    


def original():
    # creating Tk window
    master = Tk()

    # creating a Frame which can expand according
    # to the size of the window
    pane = Frame(master)
    pane.pack(fill = BOTH, expand = True)

    # button widgets which can also expand and fill
    # in the parent widget entirely
    # Button 1
    b1 = Button(pane, text = "Click me !")
    b1.pack(fill = BOTH, expand = False)

    # Button 2
    b2 = Button(pane, text = "Click me too")
    b2.pack(fill = BOTH, expand = True)

    # Execute Tkinter
    master.mainloop()

""" def drawImage(image_path):
    if not image_path:
        image_path = "example.png"

    img = PhotoImage(file=image_path)

    image_window = ScrollableImage(
        widgets['image_pane'], image=img, scrollbarwidth=6, width=200, height=200)

    image_window.pack(fill='both', expand=1)
 """

if __name__ == "__main__":
    init()
#    panedWindow()
    checkdb()
    createTwoPanes1_1()

else:
    original()

logger.info ("From packExample.py")