# import all the functionality of tkinter.
from tkinter import *
# create a window as root using Tk() function
root = Tk()
root.geometry("200x200")
# create a changecolor function
# to change background color of window


def changecolor(event):
    # get selected list box color item
    color = listbox.get(ANCHOR)
    # configure color to the window
    root.configure(bg=color)


# create listbox
listbox = Listbox(root, font=('times 20 bold'), height=5, width=10, selectmode='single')
# insert color items into listbox
listbox.insert(1, 'pink')
listbox.insert(2, 'skyblue')
listbox.insert(3, 'green2')
listbox.insert(4, 'red')
listbox.pack(pady=30)
# bind the click event with listbox and
# call changecolor() function
listbox.bind('<<ListboxSelect>>', changecolor)
root.mainloop()
